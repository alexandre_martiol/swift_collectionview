//
//  ViewController.swift
//  CollectionView
//
//  Created by AlexM on 08/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imgToShow = [UIImage (named: "chuck-norris-1.jpg"), UIImage (named: "chuck-norris-2.jpg")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //Devuelve el número de elementos que se mostrarán en el collectionView
        return 66
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        //Devuelve la celda que se debe mostrar en la posición indexPath para el collectionView. Puedes usar los siguientes pasos:
            
        //Crea una variable cell de tipo UICollectionViewCell con el método dequeueReusableCellWithReuseIdentifier del collectionView.
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCell", forIndexPath: indexPath) as UICollectionViewCell
        
        //Crea una UIImageView usando el método cell.viewWithTag(1)
        var imageView = cell.viewWithTag(1) as UIImageView
        
        //Añade una imagen al UIImageView en función del indexPath. Usa el NSArray de imágenes que habías creado.
        if (indexPath.row % 2) == 0 {
            imageView.image = imgToShow[0]
        }
        
        else {
            imageView.image = imgToShow[1]
        }
        
        return cell
    }

}

